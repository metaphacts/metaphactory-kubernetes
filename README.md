# metaphactory deployments with kubernetes

This repository has been deprecated.

All content has moved to https://github.com/metaphacts/metaphactory-kubernetes

For existing installations you may use the following commands to switch your local clone to the new location:

```
git remote set-url origin https://github.com/metaphacts/metaphactory-kubernetes.git
git fetch
```

In case of questions, do not hesitate to contact support@metaphacts.com